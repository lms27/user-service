drop table user_dtl if exists;
drop sequence if exists user_sequence;

create sequence user_sequence start with 1 increment by 1;
create table user_dtl (id bigint not null, email varchar(255), firstName varchar(255), lastName varchar(255), photo varchar(255), primary key (id));